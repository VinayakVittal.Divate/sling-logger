// import { displayLogDetails } from "./start";
import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import activeLogo from "./search-active.png";
import inactiveLogo from "./search-unfocused-active.png";
export const handleSave = () => {
  const content = document.getElementById("scrollable-div").innerText;

  const now = new Date();
  const day = String(now.getDate()).padStart(2, "0");
  const month = String(now.getMonth() + 1).padStart(2, "0");
  let hours = now.getHours();
  const minutes = String(now.getMinutes()).padStart(2, "0");
  const seconds = String(now.getSeconds()).padStart(2, "0");
  hours = hours % 12;
  hours = hours ? hours : 12;
  const formattedTime = `${String(hours).padStart(
    2,
    "0"
  )};${minutes};${seconds}`;
  const formattedDateTime = ` ${day}-${month} ${formattedTime}`;
  const filename = `logs${formattedDateTime}.txt`;

  const blob = new Blob([content], { type: "text/plain;charset=utf-8" });
  const link = document.createElement("a");
  link.href = URL.createObjectURL(blob);
  link.download = filename;
  link.click();
};
const handleReset = () => {
  document.getElementById("scrollable-div").innerHTML = "";
};
function escapeRegExp(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // Escape special characters
}
export function filterContent() {
  const searchInput = escapeRegExp(
    document.getElementById("search-input").value?.trim()
  ).toLowerCase();
  const logContent = document.getElementById("scrollable-div");
  const items = logContent.getElementsByTagName("p");
  if (searchInput === "") {
    Array.from(items).forEach(function (item) {
      if (item.classList.contains("start")) {
        return;
      }
      item.innerHTML = item.textContent;
      item.style.display = "";
    });
    return;
  }

  Array.from(items).forEach(function (item) {
    if (item.classList.contains("start")) {
      return;
    }
    const originalText = item.textContent;
    if (searchInput === "") {
      item.innerHTML = item.textContent;
      item.style.display = "";
    } else {
      const regex = new RegExp(`(${searchInput})`, "gi");
      const highlightedText = originalText.replace(
        regex,
        (match) => `<span class="highlight">${match}</span>`
      );
      item.innerHTML = highlightedText;

      if (regex.test(originalText)) {
        item.style.display = "";
      } else {
        item.style.display = "none";
      }
    }
  });
}

const LogWindow = () => {
  const [isFocused, setIsFocused] = useState(false);

  const handleFocus = () => {
    setIsFocused(true);
  };
  const handleBlur = () => {
    setIsFocused(false);
  };
  function handleDown() {
    const scrollableDiv = document.getElementById("scrollable-div");
    scrollableDiv.scrollTop = scrollableDiv.scrollHeight;
  }
  return (
    <>
      <div className="window-container" id="wc">
        <div className="scrollable-div-label">
          <span>Logs</span>

          <span>
            <FontAwesomeIcon
              className="down"
              icon={faChevronDown}
              onClick={handleDown}
            />

            <button id="reset" onClick={handleReset}>
              Clear
            </button>
          </span>
        </div>
        <div className="search-container">
          <input
            type="text"
            id="search-input"
            placeholder="keyword..."
            onFocus={handleFocus}
            onBlur={handleBlur}
            onChange={filterContent}
          />
          <img
            src={isFocused ? activeLogo : inactiveLogo}
            alt="Search"
            className="searchLogo"
          />
        </div>
        <div id="download" onClick={handleSave}></div>
        <div className="scrollable-div" id="scrollable-div"></div>
      </div>
    </>
  );
};

export default LogWindow;
