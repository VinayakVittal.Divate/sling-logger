export function showYesNoPrompt(message, callback) {
  const modal = document.getElementById("yesNoCancelModal");
  const modalMessage = document.getElementById("modalMessage");
  const yesButton = document.getElementById("yesButton");
  const noButton = document.getElementById("noButton");
  const cancelButton = document.getElementById("cancelButton");

  modalMessage.textContent = message;

  modal.style.display = "flex";

  yesButton.onclick = function () {
    callback("yes");
    modal.style.display = "none";
  };

  noButton.onclick = function () {
    callback("no");
    modal.style.display = "none";
  };

  cancelButton.onclick = function () {
    callback("ignore");
    modal.style.display = "none";
  };
}
