function createSpanElements(array) {
  const fragment = document.createDocumentFragment();
  array.forEach((item, index) => {
    const span = document.createElement("span");
    span.textContent = item;
    fragment.appendChild(span);
    if (index < array.length - 1) {
      fragment.appendChild(document.createTextNode(" "));
    }
  });
  return fragment;
}

export function displayLogDetails(
  selectedLogLevels,
  selectedLogModules
  // platform
) {
  const scrollableDiv = document.getElementById("scrollable-div");
  const shouldScroll =
    scrollableDiv.scrollTop + scrollableDiv.clientHeight >=
    scrollableDiv.scrollHeight - 1;
  if (
    scrollableDiv.lastElementChild &&
    scrollableDiv.lastElementChild.classList.contains("start")
  ) {
    scrollableDiv.removeChild(scrollableDiv.lastElementChild);
  } else {
    const title = document.createElement("h3");
    title.classList.add("title");
    title.innerText = "Filters";
    scrollableDiv.appendChild(title);
  }

  const logElement = document.createElement("p");
  logElement.classList.add("start");

  const logLevelsHeading = document.createElement("strong");
  logLevelsHeading.textContent = "LogLevels: ";
  logElement.appendChild(logLevelsHeading);
  logElement.appendChild(createSpanElements(selectedLogLevels));

  const logModulesHeading = document.createElement("strong");
  logModulesHeading.textContent = " LogModules: ";
  logElement.appendChild(logModulesHeading);
  logElement.appendChild(createSpanElements(selectedLogModules));

  // const platformHeading = document.createElement("strong");
  // platformHeading.textContent = " Destination: ";
  // logElement.appendChild(platformHeading);
  // const platformSpan = document.createElement("span");
  // platformSpan.textContent = platform;
  // logElement.appendChild(platformSpan);

  scrollableDiv.appendChild(logElement);
  if (shouldScroll) {
    scrollableDiv.scrollTop = scrollableDiv.scrollHeight;
  }
}
