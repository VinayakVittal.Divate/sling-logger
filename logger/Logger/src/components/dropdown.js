import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { faDotCircle } from "@fortawesome/free-solid-svg-icons";

import { useRef, useState, useEffect } from "react";

function Dropdown({ title, items, containerClass, checks, setChecks, onBlur }) {
  const dropdownRef = useRef(null);
  const [btn, setBtn] = useState(false);

  function handleDropdown() {
    setBtn((prevBtn) => !prevBtn);
  }

  function handleSelect(index) {
    if (items[0] !== "Default")
      setChecks((prevChecks) => {
        const newChecks = prevChecks.map((check, i) =>
          i === index ? true : false
        );
        return newChecks;
      });
    else
      setChecks((prevChecks) => {
        let newChecks = [...prevChecks];
        if (newChecks[0] === true) {
          newChecks[0] = false;
        } else if (items[index] === "Default") {
          newChecks = newChecks.map(() => false);
        }
        newChecks[index] = !newChecks[index];
        return newChecks;
      });
  }

  useEffect(() => {
    function handleClickOutside(event) {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
        setBtn(false);
      }
    }

    document.addEventListener("mousedown", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  useEffect(() => {
    const allUnchecked = checks.every((check) => !check);
    if (allUnchecked) {
      setChecks((prevChecks) => {
        const newChecks = [...prevChecks];
        newChecks[0] = true;
        return newChecks;
      });
    }
  }, [checks, setChecks]);

  return (
    <div
      className="dropdown-container"
      ref={dropdownRef}
      onBlur={onBlur}
      tabIndex="0"
    >
      <div
        className={`select-btn${btn ? " open" : ""} ${containerClass}dropdown`}
        onClick={handleDropdown}
      >
        <span className="btn-text">{title}</span>
        <span className="arrow-dwn">
          <FontAwesomeIcon icon={faChevronDown} />
        </span>
      </div>
      <ul className="list-items">
        {items.map((element, index) => (
          <li
            key={index}
            className={`item${checks[index] ? " checked" : ""}${
              element === "Default" ? " line" : ""
            }`}
            onClick={() => handleSelect(index)}
          >
            <span
              className={containerClass === "DT" ? "dt-checkbox" : "checkbox"}
            >
              <FontAwesomeIcon
                icon={containerClass === "DT" ? faDotCircle : faCheck}
                className="check-icon"
              />
            </span>
            <span className="item-text">{element}</span>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default Dropdown;
