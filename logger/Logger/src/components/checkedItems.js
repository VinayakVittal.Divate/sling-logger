const CheckedItems = ({ items, checks, containerClass }) => {
  return (
    <div className={containerClass}>
      <span>{`${containerClass}${checks[0] ? ": Default" : ""}`}</span>
      <ul className="checked-items">
        {items.map((ele, index) => {
          if (checks[index]) {
            if (ele === "Default") {
              checks = checks.map((ele, index) => {
                if (index !== 0) return true;
                else return false;
              });
              return null;
            } else return <li key={index}>{ele}</li>;
          } else return null;
        })}
      </ul>
    </div>
  );
};
export default CheckedItems;
