import React, { useState, useEffect, useRef } from "react";
function extractIPorHostname(url) {
  const regex = /^(?:http:\/\/)?([a-zA-Z0-9.-]+)(?::\d+)?/;
  const match = url.match(regex);
  return match ? match[1] : null;
}
export const compareIps = (current, latest, ip) => {
  current = extractIPorHostname(current);
  latest = extractIPorHostname(latest);
  ip = extractIPorHostname(ip);
  if (!ip) return false;
  if (current && current.includes("localhost")) {
    current = ip;
  }
  if (latest && latest.includes("localhost")) {
    latest = ip;
  }
  if (current !== latest) {
    return true;
  }
  return false;
};
const Ipaddress = ({ port, connect }) => {
  const [ipAddress, setIpAddress] = useState("");
  const intervalIdRef = useRef(null);

  useEffect(() => {
    const fetchIpAddress = async () => {
      try {
        const response = await fetch(`http://localhost:${port}/api/ip`);
        if (!response.ok) {
          throw new Error("Failed to fetch IP address");
        }
        const data = await response.json();
        setIpAddress(data.ip);
      } catch (error) {
        console.error("Error fetching the IP address:", error);
      }
    };

    if (port && connect[1] !== "Connect") {
      fetchIpAddress();
      if (!intervalIdRef.current) {
        intervalIdRef.current = setInterval(fetchIpAddress, 5000);
      }
    } else {
      if (intervalIdRef.current) {
        clearInterval(intervalIdRef.current);
        intervalIdRef.current = null;
      }
    }

    return () => {
      if (intervalIdRef.current) {
        clearInterval(intervalIdRef.current);
        intervalIdRef.current = null;
      }
    };
  }, [port, connect]);

  return (
    <div className="ip-address-container">
      {ipAddress && (
        <>
          <span>Your IP Address:</span>
          <p>
            {ipAddress[0]}:{port}
          </p>
        </>
      )}
    </div>
  );
};
export default Ipaddress;
