import { useEffect, useRef, useState } from "react";
import CheckedItems from "./checkedItems";
import Dropdown from "./dropdown";
import { displayLogDetails } from "./start";
import Ipaddress, { compareIps } from "./ipaddress";
import { showYesNoPrompt } from "./modal";
import { filterContent, handleSave } from "./logWindow";
import logo from "./slinglogoLarge.png";
const LogM = [
  "Default",
  "UI",
  "Player",
  "Navigation",
  "AirTV",
  "Analytics",
  "Bridge",
];
const LogL = ["Default", "info", "debug", "error", "warn"];
const Dest = ["this Monitor", "TV console", "APJS server"];

function Form() {
  const [checksLM, setChecksLM] = useState([
    true,
    ...Array(LogM.length - 1).fill(false),
  ]);
  const [checksLL, setChecksLL] = useState([
    true,
    ...Array(LogL.length - 1).fill(false),
  ]);
  const [checksDT, setChecksDT] = useState([
    true,
    ...Array(Dest.length - 1).fill(false),
  ]);
  const [Ip, setIp] = useState("");
  const [oldLM, setOldLM] = useState([
    true,
    ...Array(LogM.length - 1).fill(false),
  ]);
  const [oldLL, setOldLL] = useState([
    true,
    ...Array(LogL.length - 1).fill(false),
  ]);
  const [oldDT, setOldDT] = useState([
    true,
    ...Array(Dest.length - 1).fill(false),
  ]);
  const [oldIp, setOldIp] = useState("");
  const [connect, setConnect] = useState(["Connect", "Connect"]);
  const [port, setPort] = useState(null);
  const [shouldSubmit, setShouldSubmit] = useState(false);
  const RefDT = useRef(checksDT);
  const RefLL = useRef(checksLL);
  const RefLM = useRef(checksLM);
  const oldRefDT = useRef(oldDT);
  const oldRefLL = useRef(oldLL);
  const oldRefLM = useRef(oldLM);
  const oldRefIp = useRef(oldIp);
  const ipRef = useRef(Ip);
  const dropdownRef = useRef(null);
  const connectButtonRef = useRef(null);

  useEffect(() => {
    RefLM.current = checksLM;
  }, [checksLM]);

  useEffect(() => {
    RefLL.current = checksLL;
  }, [checksLL]);

  useEffect(() => {
    RefDT.current = checksDT;
  }, [checksDT]);

  useEffect(() => {
    ipRef.current = Ip;
  }, [Ip]);

  useEffect(() => {
    oldRefDT.current = oldDT;
  }, [oldDT]);

  useEffect(() => {
    oldRefLL.current = oldLL;
  }, [oldLL]);

  useEffect(() => {
    oldRefLM.current = oldLM;
  }, [oldLM]);
  useEffect(() => {
    oldRefIp.current = oldIp;
  }, [oldIp]);

  useEffect(() => {
    if (shouldSubmit && port) {
      submitForm();
      setShouldSubmit(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [port, shouldSubmit]);
  function isEqual() {
    return (
      JSON.stringify(RefDT.current) === JSON.stringify(oldRefDT.current) &&
      JSON.stringify(RefLL.current) === JSON.stringify(oldRefLL.current) &&
      JSON.stringify(RefLM.current) === JSON.stringify(oldRefLM.current) &&
      ipRef.current === oldRefIp.current
    );
  }
  function handleIpChange(e) {
    setIp(e.target.value);
  }
  function handleBlur(e) {
    if (
      oldRefIp.current &&
      !dropdownRef.current.contains(e.relatedTarget) &&
      connectButtonRef.current !== e.relatedTarget &&
      e.target.value !== oldRefIp.current
    )
      setIp(oldIp);
  }
  const handleReset = () => {
    document.getElementById("scrollable-div").innerHTML = "";
  };
  function saveOrClear() {
    document.querySelector(".labelline").style.zIndex = "0";
    showYesNoPrompt(
      "Do you want to Save or clear the existing logs",
      function (response) {
        document.querySelector(".labelline").style.zIndex = "1111";
        if (response === "yes") {
          handleSave();
          handleReset();
        } else if (response === "no") {
          handleReset();
        } else if (response === "ignore") {
        }
      }
    );
  }
  useEffect(() => {
    const connectButton = document.getElementById("connect");
    if (connect[0] === "connected") {
      connectButton.style.backgroundColor = "rgb(8, 210, 66)";
      connectButton.style.borderColor = "rgb(63, 255, 0)";
    } else {
      connectButton.style.backgroundColor = "";
      connectButton.style.borderColor = "";
    }
  }, [connect]);
  useEffect(() => {
    if (!isEqual()) {
      setConnect((prev) => ["Connect", prev[1]]);
    } else setConnect((prev) => [prev[1], prev[1]]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [checksDT, checksLL, checksLM, Ip, oldIp, oldDT, oldLL, oldLM]);
  const handleFormSubmit = async (event) => {
    event.preventDefault();
    const isConnected = await checkConnectionStatus();
    if (!port || !isConnected) {
      const userPort = prompt("Enter the port of the Server:");
      if (!userPort) {
        alert("Port is required.");
        return;
      }
      setPort(userPort);
      setShouldSubmit(true);
    } else {
      submitForm();
    }
  };
  function developFilters() {
    const LogLs = [];
    const LogMs = [];
    let Dests = [];
    checksLM.forEach((isChecked, index) => {
      if (isChecked) {
        if (index === 0) {
          LogM.forEach((ele) => LogMs.push(ele));
          LogMs.shift();
        } else {
          LogMs.push(LogM[index]);
        }
      }
    });

    checksLL.forEach((isChecked, index) => {
      if (isChecked) {
        if (index === 0) {
          LogL.forEach((ele) => LogLs.push(ele));
          LogLs.shift();
        } else {
          LogLs.push(LogL[index]);
        }
      }
    });

    checksDT.forEach((isChecked, index) => {
      if (isChecked) {
        Dests = Dest[index];
      }
    });
    return { LogLs, LogMs, Dests };
  }
  useEffect(() => {
    if (
      document.getElementById("scrollable-div").innerHTML === "" &&
      connect[1] !== "Connect"
    ) {
      const { LogLs, LogMs, Dests } = developFilters();
      displayLogDetails(LogLs, LogMs, Dests);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [connect]);

  const submitForm = async () => {
    if (connect[0] !== "Connect") return;
    const deviceIP = ipRef.current;
    console.log(deviceIP);
    const ip = document.querySelector(".ip-address-container p")?.textContent;
    if (
      connect[1] !== "Connect" &&
      compareIps(ipRef.current, oldRefIp.current, ip)
    ) {
      saveOrClear();
      try {
        const response = await fetch(`http://localhost:${port}/disconnect`, {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        });
        if (response.ok) {
          const data = await response.json();
          console.log(data);
        } else {
          console.error("Failed to disconnect socket:", response.status);
        }
      } catch (error) {
        console.error("Error occurred while disconnecting socket:", error);
      }
    }
    setOldLM([...checksLM]);
    setOldLL([...checksLL]);
    setOldDT([...checksDT]);
    setOldIp(Ip);

    const { LogLs, LogMs, Dests } = developFilters();

    try {
      const response = await fetch(`http://localhost:${port}/config`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          deviceIP: deviceIP,
          allowedLogs: LogLs,
          allowedModules: LogMs,
          platform: Dests,
        }),
      });

      if (response.ok) {
        console.log("Configuration data sent successfully!");
        const errElement = document.getElementById("err");
        if (errElement) {
          errElement.classList.add("hidden");
        }
        setConnect(["Connecting", "Connecting"]);
        const wcElement = document.getElementById("wc");
        if (wcElement) {
          wcElement.classList.remove("hidden");
        }

        displayLogDetails(LogLs, LogMs, Dests);
        fetchLogsAndUpdate();
      } else {
        console.error("Failed to send configuration data.");
        const errElement = document.getElementById("err");
        if (errElement) {
          errElement.classList.remove("hidden");
          errElement.classList.add("err");
        }
      }
    } catch (error) {
      console.error("Error:", error);
      const errElement = document.getElementById("err");
      if (errElement) {
        errElement.classList.remove("hidden");
        errElement.classList.add("err");
      }
      const wcElement = document.getElementById("wc");
      if (wcElement) {
        wcElement.classList.add("hidden");
      }
    }
  };
  async function checkConnectionStatus() {
    try {
      const response = await fetch(
        `http://localhost:${port}/connection-status`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      const scrollableDiv = document.getElementById("scrollable-div");
      const logElement = document.createElement("p");

      const data = await response.json();
      setConnect((prev) => {
        if (data.status === "connected") {
          // if (prev[1] !== "connected") {
          //   logElement.classList.add("Connected");
          //   logElement.textContent = `App Connected`;
          //   scrollableDiv.appendChild(logElement);
          // }
          if (prev[0] !== prev[1]) return [prev[0], "connected"];
          return ["connected", "connected"];
        } else if (data.status === "disconnected") {
          if (prev[1] === "connected") {
            setTimeout(() => {
              const shouldScroll =
                scrollableDiv.scrollTop + scrollableDiv.clientHeight >=
                scrollableDiv.scrollHeight - 1;
              logElement.classList.add("Disconnected");
              logElement.textContent = `App Disconnected`;
              scrollableDiv.appendChild(logElement);
              if (shouldScroll)
                scrollableDiv.scrollTop = scrollableDiv.scrollHeight;
            }, 2000);
          }
          if (prev[0] !== prev[1]) return [prev[0], "Connecting"];
          return ["Connecting", "Connecting"];
        }
        return prev;
      });
      return true;
    } catch (error) {
      setConnect(["Connect", "Connect"]);
      return false;
      // console.error("There was a problem with the fetch operation:", error);
    }
  }

  function fetchLogsAndUpdate() {
    checkConnectionStatus();
    fetch(`http://localhost:${port}/logs`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Connection lost");
        }
        return response.json();
      })
      .then((data) => {
        const scrollableDiv = document.getElementById("scrollable-div");
        const shouldScroll =
          scrollableDiv.scrollTop + scrollableDiv.clientHeight >=
          scrollableDiv.scrollHeight - 1;
        scrollableDiv.style.scrollBehavior = "auto";
        const fragment = document.createDocumentFragment();
        data.forEach((log) => {
          const logElement = document.createElement("p");
          logElement.classList.add(log.type);
          logElement.textContent = `${log.message}`;
          fragment.appendChild(logElement);
        });
        scrollableDiv.appendChild(fragment);
        filterContent();
        if (shouldScroll) {
          scrollableDiv.scrollTop = scrollableDiv.scrollHeight;
        }
        scrollableDiv.style.scrollBehavior = "smooth";
        setTimeout(fetchLogsAndUpdate, 1000);
      })
      .catch((error) => {
        console.error("Error fetching logs:", error);
        console.log("Disconnected");
      });
  }

  return (
    <form id="configForm" onSubmit={handleFormSubmit}>
      <div className="container">
        <div className="inputcontainer input-text">
          <div className="entryarea">
            <input
              onChange={(e) => handleIpChange(e)}
              onBlur={(e) => handleBlur(e)}
              type="text"
              id="deviceIP"
              list="deviceIPs"
              name="deviceIP"
              autoComplete="on"
              value={Ip}
              required
            />
            <div className="labelline">Enter device's IP address</div>
            <datalist id="deviceIPs">
              <option value="http://localhost:8080"></option>
            </datalist>
          </div>
        </div>
      </div>
      <hr className="input-divider" />
      <div className="dropdowns-container" ref={dropdownRef}>
        <Dropdown
          title="Log Modules"
          items={LogM}
          containerClass="LM"
          checks={checksLM}
          setChecks={setChecksLM}
          onBlur={(e) => handleBlur(e)}
        />
        <Dropdown
          title="Log Levels"
          items={LogL}
          containerClass="LL"
          checks={checksLL}
          setChecks={setChecksLL}
          onBlur={(e) => handleBlur(e)}
        />
        <Dropdown
          title="Destination"
          items={Dest}
          containerClass="DT"
          checks={checksDT}
          setChecks={setChecksDT}
          onBlur={(e) => handleBlur(e)}
        />
      </div>
      <div className="checked-items-container">
        <CheckedItems
          items={LogM}
          checks={checksLM}
          containerClass="Log Modules"
        />
        <CheckedItems
          items={LogL}
          checks={checksLL}
          containerClass="Log Levels"
        />
        <CheckedItems
          items={Dest}
          checks={checksDT}
          containerClass="Destination"
        />
      </div>

      <button type="submit" id="connect" ref={connectButtonRef}>
        {connect[0]}
      </button>
      <div>
        <Ipaddress port={port} connect={connect} />
        <img src={logo} alt="Logo" className="logo" />
      </div>
    </form>
  );
}
export default Form;
