import "./App.css";
import Form from "./components/form";
import LogWindow from "./components/logWindow";

function App() {
  window.onload = () => {
    document.querySelector(".window-container").classList.add("hidden");
  };
  return (
    <div className="total">
      <Form />
      <div id="err" className="hidden">
        <span>⚠️ Couldn't Connect</span>
      </div>
      <LogWindow />
      <footer className="footer">
        <div className="footer-left">© 2024 Sling TV L.L.C.</div>
        <div className="footer-right">
          <a
            href="https://www.sling.com/offer-details/disclaimers/terms-of-use?footer=true"
            target="_blank"
            rel="noopener noreferrer"
          >
            Terms of Use
          </a>{" "}
          |
          <a
            href="https://www.sling.com/offer-details/disclaimers/privacy-notice"
            target="_blank"
            rel="noopener noreferrer"
          >
            Privacy Policy
          </a>{" "}
          |
          <a
            href="https://www.sling.com/offer-details/disclaimers/legal-notices?footer=true"
            target="_blank"
            rel="noopener noreferrer"
          >
            Legal Notices
          </a>
        </div>
      </footer>
    </div>
  );
}

export default App;
