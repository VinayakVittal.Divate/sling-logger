var Server = require("socket.io").Server;
var http = require("http");
var express = require("express");
var cors = require("cors");
var readline = require("readline");
var fs = require("fs");
var net = require("net");
var os = require("os");

var app = express();
var server = http.createServer(app);
var allowedOrigin = null,
  allowedLogs,
  allowedModules,
  Platform;
var logs = [];
var socketConnected = false;
var lastConnectionCheck = Date.now();
var CONNECTION_TIMEOUT = 60000;

var getIPAddresses = function () {
  var interfaces = os.networkInterfaces();
  return Object.values(interfaces)
    .reduce(function (acc, iface) {
      return acc.concat(iface);
    }, [])
    .filter(function (iface) {
      return iface.family === "IPv4" && !iface.internal;
    })
    .map(function (iface) {
      return iface.address;
    });
};

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

var io = new Server(server, {
  cors: {
    origin: function (origin, callback) {
      if (allowedOrigin) {
        callback(null, origin);
      } else {
        callback(new Error("Origin not allowed"));
      }
    },
    methods: ["GET", "POST"],
    transports: ["websocket", "polling"],
    credentials: true,
    allowedHeaders: ["Content-Type"],
  },
  allowEIO3: true,
});

io.use(function (socket, next) {
  var clientIp = socket.handshake.address;
  if (clientIp.startsWith("::ffff:")) {
    clientIp = clientIp.split(":").pop();
  }
  var deviceIps = getIPAddresses();
  if (allowedOrigin && allowedOrigin.includes("localhost")) {
    if (deviceIps.length > 0) {
      allowedOrigin = deviceIps[0];
    }
  }
  if (clientIp === "127.0.0.1") {
    if (deviceIps.length > 0) {
      clientIp = deviceIps[0];
    }
  }
  if (clientIp !== allowedOrigin) {
    console.log("Unauthorized connection attempt from IP:", clientIp);
    return next(new Error("Unauthorized connection"));
  }
  return next();
});

io.on("connection", function (socket) {
  console.log("A client connected");
  socketConnected = true;
  lastConnectionCheck = Date.now();
  socket.emit("initialData", {
    allowedLogs: allowedLogs,
    allowedModules: allowedModules,
    Platform: Platform,
  });

  socket.on("debug", function (message) {
    console.log("[DEBUG]", message);
    logs.push({ type: "DEBUG", message: message });
  });

  socket.on("info", function (message) {
    console.log("[INFO]", message);
    logs.push({ type: "INFO", message: message });
  });

  socket.on("warn", function (message) {
    console.log("[WARN]", message);
    logs.push({ type: "WARN", message: message });
  });

  socket.on("error", function (message) {
    console.log("[ERROR]", message);
    logs.push({ type: "ERROR", message: message });
  });

  socket.on("disconnect", function () {
    socketConnected = false;
    console.log("Client disconnected");
  });
});

app.post("/config", function (req, res) {
  allowedOrigin = req.body.deviceIP;
  allowedLogs = req.body.allowedLogs;
  allowedModules = req.body.allowedModules;
  Platform = req.body.platform;
  console.log(req.body);

  io.emit("initialData", {
    allowedLogs: allowedLogs,
    allowedModules: allowedModules,
    Platform: Platform,
  });
  res.sendStatus(200);
});

app.get("/logs", function (req, res) {
  res.json(logs);
  logs = [];
});

app.get("/connection-status", function (req, res) {
  lastConnectionCheck = Date.now();
  if (socketConnected) {
    res.json({ status: "connected" });
  } else {
    res.json({ status: "disconnected" });
  }
});

app.get("/disconnect", function (req, res) {
  if (socketConnected) {
    io.sockets.disconnectSockets();
    socketConnected = false;
    allowedOrigin = null;
    res.json({ status: "disconnected" });
  } else {
    res.json({ status: "already disconnected" });
  }
});

app.get("/api/ip", function (req, res) {
  var currentIPs = getIPAddresses();
  res.json({ ip: currentIPs });
});

setInterval(function () {
  if (Date.now() - lastConnectionCheck > CONNECTION_TIMEOUT) {
    if (socketConnected) {
      console.log("Connection status request timeout. Disconnecting client...");
      io.sockets.disconnectSockets();
      socketConnected = false;
      allowedOrigin = null;
    }
  }
}, CONNECTION_TIMEOUT);

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

var checkPortAvailability = function (port, callback) {
  var tester = net
    .createServer()
    .once("error", function (err) {
      if (err.code === "EADDRINUSE") {
        callback(false);
      } else {
        callback(err);
      }
    })
    .once("listening", function () {
      tester
        .once("close", function () {
          callback(true);
        })
        .close();
    })
    .listen(port);
};

var promptForPort = function () {
  rl.question("Enter Server port number:(5000) ", function (portInput) {
    var port;
    if (portInput === "") port = 5000;
    else port = parseInt(portInput, 10);
    if (isNaN(port)) {
      console.log("Invalid port number. Please enter a valid number.");
      promptForPort();
    } else {
      checkPortAvailability(port, function (isAvailable) {
        if (isAvailable) {
          server.listen(port, function () {
            console.log("Server is running at http://localhost:" + port);
          });
          fs.writeFileSync("server-ready.txt", "ready");
          rl.close();
        } else {
          console.log(
            "Port " +
              port +
              " is already in use. Please enter another port number."
          );
          promptForPort();
        }
      });
    }
  });
};

promptForPort();
