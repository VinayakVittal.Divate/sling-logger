const { spawn } = require("child_process");
const path = require("path");

const reactAppPath = path.join(__dirname, "helper.js");
const serverAppPath = path.join(__dirname, "server.js");

const startApp = (scriptPath, name) => {
  const child = spawn("node", [scriptPath], {
    stdio: "inherit", // Ensures the child process uses the parent process's stdio
  });

  child.on("close", (code) => {
    console.log(`${name} process exited with code ${code}`);
  });
};

// Start both applications
startApp(serverAppPath, "Server App");
startApp(reactAppPath, "React App");
