const express = require("express");
const path = require("path");
const detectPort = require("detect-port");
const open = require("open");
const fs = require("fs");

const DEFAULT_PORT = 3000;

const app = express();

app.use(express.static(path.join(__dirname, "build")));

app.get("/*", function (req, res) {
  res.sendFile(path.join(__dirname, "build", "index.html"));
});

detectPort(DEFAULT_PORT, (err, availablePort) => {
  if (err) {
    console.error(`Error detecting port: ${err}`);
    return;
  }

  const port = availablePort === DEFAULT_PORT ? DEFAULT_PORT : availablePort;

  app.listen(port, () => {
    console.log(`Logger App is running on port ${port}`);
    waitForServerReady(port);
  });
});
function waitForServerReady(port) {
  const checkInterval = 1000; // Check every second
  const checkServerReady = () => {
    fs.readFile("server-ready.txt", "utf8", (err, data) => {
      if (!err && data === "ready") {
        const filePath = "server-ready.txt";

        fs.unlink(filePath, (err) => {
          if (err) {
            console.error(`Error deleting ${filePath}:`, err);
          }
        });
        open(`http://localhost:${port}`);
      } else {
        setTimeout(checkServerReady, checkInterval);
      }
    });
  };
  checkServerReady();
}
// npm run build
// npx pkg .
// for windows
// "pkg": "pkg . --targets node14-win-x64, --output app-win"
