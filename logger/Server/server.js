import { Server } from "socket.io";
import http from "http";
import express from "express";
import cors from "cors";
import readline from "readline";
import net from "net";
import os from "os";

const app = express();
const server = http.createServer(app);
let allowedOrigin = null,
  allowedLogs,
  allowedModules,
  Platform;
let logs = [];
let socketConnected = false;
let lastConnectionCheck = Date.now();
const CONNECTION_TIMEOUT = 60000;

var getIPAddresses = function () {
  var interfaces = os.networkInterfaces();
  return Object.values(interfaces)
    .reduce(function (acc, iface) {
      return acc.concat(iface);
    }, [])
    .filter(function (iface) {
      return iface.family === "IPv4" && !iface.internal;
    })
    .map(function (iface) {
      return iface.address;
    });
};

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const io = new Server(server, {
  cors: {
    origin: (origin, callback) => {
      if (allowedOrigin) {
        callback(null, origin);
      } else {
        callback(new Error("Origin not allowed"));
      }
    },
    methods: ["GET", "POST"],
    transports: ["websocket", "polling"],
    credentials: true,
    allowedHeaders: ["Content-Type"],
  },
  allowEIO3: true,
});
io.use((socket, next) => {
  let clientIp = socket.handshake.address;
  if (clientIp.startsWith("::ffff:")) {
    clientIp = clientIp.split(":").pop();
  }
  const deviceIps = getIPAddresses();
  if (allowedOrigin && allowedOrigin.includes("localhost")) {
    if (deviceIps.length > 0) {
      allowedOrigin = deviceIps[0];
    }
  }
  if (clientIp === "127.0.0.1") {
    if (deviceIps.length > 0) {
      clientIp = deviceIps[0];
    }
  }
  if (clientIp !== allowedOrigin) {
    console.log("Unauthorized connection attempt from IP:", clientIp);
    return next(new Error("Unauthorized connection"));
  }
  return next();
});

io.on("connection", (socket) => {
  console.log("A client connected");
  socketConnected = true;
  lastConnectionCheck = Date.now();
  socket.emit("initialData", { allowedLogs, allowedModules, Platform });

  socket.on("debug", (message) => {
    console.log("[DEBUG]", message);
    logs.push({ type: "DEBUG", message: message });
  });

  socket.on("info", (message) => {
    console.log("[INFO]", message);
    logs.push({ type: "INFO", message: message });
  });

  socket.on("warn", (message) => {
    console.log("[WARN]", message);
    logs.push({ type: "WARN", message: message });
  });

  socket.on("error", (message) => {
    console.log("[ERROR]", message);
    logs.push({ type: "ERROR", message: message });
  });

  socket.on("disconnect", () => {
    socketConnected = false;
    console.log("Client disconnected");
  });
});

app.post("/config", (req, res) => {
  allowedOrigin = req.body.deviceIP;
  allowedLogs = req.body.allowedLogs;
  allowedModules = req.body.allowedModules;
  Platform = req.body.platform;
  console.log(req.body);

  io.emit("initialData", { allowedLogs, allowedModules, Platform });
  res.sendStatus(200);
});

app.get("/logs", (req, res) => {
  res.json(logs);
  logs = [];
});

app.get("/connection-status", (req, res) => {
  lastConnectionCheck = Date.now();
  if (socketConnected) {
    res.json({ status: "connected" });
  } else {
    res.json({ status: "disconnected" });
  }
});

app.get("/disconnect", (req, res) => {
  if (socketConnected) {
    io.sockets.disconnectSockets();
    socketConnected = false;
    allowedOrigin = null;
    res.json({ status: "disconnected" });
  } else {
    res.json({ status: "already disconnected" });
  }
});

app.get("/api/ip", function (req, res) {
  var currentIPs = getIPAddresses();
  res.json({ ip: currentIPs });
});

setInterval(() => {
  if (Date.now() - lastConnectionCheck > CONNECTION_TIMEOUT) {
    if (socketConnected) {
      console.log("Connection status request timeout. Disconnecting client...");
      io.sockets.disconnectSockets();
      socketConnected = false;
      allowedOrigin = null;
    }
  }
}, CONNECTION_TIMEOUT);

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const checkPortAvailability = (port, callback) => {
  const tester = net
    .createServer()
    .once("error", (err) => {
      if (err.code === "EADDRINUSE") {
        callback(false);
      } else {
        callback(err);
      }
    })
    .once("listening", () => {
      tester.once("close", () => callback(true)).close();
    })
    .listen(port);
};

const promptForPort = () => {
  rl.question("Enter port number:(5000) ", (portInput) => {
    let port;
    if (portInput === "") port = 5000;
    else port = parseInt(portInput, 10);
    if (isNaN(port)) {
      console.log("Invalid port number. Please enter a valid number.");
      promptForPort();
    } else {
      checkPortAvailability(port, (isAvailable) => {
        if (isAvailable) {
          server.listen(port, () => {
            console.log(`Server is running at http://localhost:${port}`);
          });
          rl.close();
        } else {
          console.log(
            `Port ${port} is already in use. Please enter another port number.`
          );
          promptForPort();
        }
      });
    }
  });
};

promptForPort();
